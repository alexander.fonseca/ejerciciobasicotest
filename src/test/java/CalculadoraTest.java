import org.junit.jupiter.api.*;

import javax.naming.SizeLimitExceededException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


class CalculadoraTest {

    private PrintStream originalSystemOut;
    private ByteArrayOutputStream systemOutContent;

    @BeforeEach
    void redirectSystemOutStream() {
        originalSystemOut = System.out;
        systemOutContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(systemOutContent));
    }

    @AfterEach
    void restoreSystemOutStream() {
        System.setOut(originalSystemOut);
    }


    @Test()
    void validateMethodSortOnlyPositiveNumbers() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Calculadora.sortPositiveNumbers(-1, -45, 5);
        });
        String messageResult = exception.getMessage();
        String messageExpected = "Este metodo solo recibe numeros positivos";
        assertEquals(messageResult, messageExpected);
    }

    @Test
    void validateMethodOnlyThreeParams() {
        SizeLimitExceededException exception = assertThrows(SizeLimitExceededException.class, () -> {
            Calculadora.sortPositiveNumbers(1, 2, 4, 5, 7, 8);
        });
        String messageResult = exception.getMessage();
        String messageExpected = "Este metodo solo recibe 3 numeros positivos";
        assertEquals(messageResult, messageExpected);
    }

    @Test
    void validateMethodSortMaxAndMinNumber() throws Exception {
        Calculadora.sortPositiveNumbers(23, 14, 29);
        String expected = "El numero mayor es 29 y el menor es 14";
        Assertions.assertEquals(expected.trim(), systemOutContent.toString().trim());
    }

    @Test
    void validateMethodCreatedArrayReturnArrayNotEmpty(){
        Integer [] numbers = Calculadora.generateArrayWithNumbersLessFive();
        Assertions.assertEquals(true,(numbers.length > 0));
    }

    @Test
    void validateMethodCreatedArrayReturnNumbersLessFive() {
        int countHigherFive;
        Integer [] numbers = Calculadora.generateArrayWithNumbersLessFive();
        countHigherFive = (int) Arrays.stream(numbers).filter(number -> number > 5).count();
        Assertions.assertEquals(false,(countHigherFive > 0));
    }

    @Test
    void validateMethodSumIsCorrect(){
        int result = Calculadora.sum(3,2);
        int expected = 5;
        Assertions.assertEquals(expected,result);
    }

    @Test
    void validateMethodSubtractIsCorrect(){
        int result = Calculadora.subtract(8,2);
        int expected = 6;
        Assertions.assertEquals(expected,result);
    }

    @Test
    void validateMethodMultiplicationIsCorrect(){
        int result = Calculadora.multiplication(3,3);
        int expected = 9;
        Assertions.assertEquals(expected,result);
    }

    @Test
    void validateMethodDivideIsCorrect(){
        int result = Calculadora.divide(6,2);
        int expected = 3;
        Assertions.assertEquals(expected,result);
    }

}