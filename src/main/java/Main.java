import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        try {
            /*
                Enunciado 1
                Escriba un programa que defina 3 variables con números enteros positivos,
                y que de esas variables calcule e imprima en pantalla el menor y el mayor.
            */
            System.out.println("Enunciado #1");
            Calculadora.sortPositiveNumbers(2, 8, 7);

            /*
            Crear un vector entero con 10 números aleatorios, después leer el vector aquellos
            valores que sean menores a 5 eliminarlos del vector y por último imprimir completamente el vector
             */
            System.out.println("Enunciado #2");
            Arrays.stream(Calculadora.generateArrayWithNumbersLessFive()).forEach(number->{
                System.out.println(number);
            });
            /*
            Construir un programa que simule el funcionamiento de una calculadora que puede realizar las cuatro
            operaciones aritméticas básicas (suma, resta, producto y división) con valores numéricos enteros.
            Donde cada operación esté en un método y pida los datos necesarios para la operación y retorne el resultado,
            ya para ejecutarlo en el método main ponga el nombre del método, los valores e imprima el resultado.
             */
            System.out.println("Enunciado #3");
            int sum = Calculadora.sum(2,2);
            System.out.println("sum = " + sum);
            int subtract = Calculadora.subtract(4,2);
            System.out.println("subtract = " + subtract);
            int multiplication = Calculadora.multiplication(2,2);
            System.out.println("multiplication = " + multiplication);
            int divide = Calculadora.divide(6,2);
            System.out.println("divide = " + divide);

        }
        catch (Exception error) {
            System.out.println(error.getMessage());
        }


    }

}
