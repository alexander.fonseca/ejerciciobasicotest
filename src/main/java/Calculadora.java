import javax.naming.SizeLimitExceededException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Calculadora {

    private static final String ERROR_ONLY_POSITIVE_NUMBERS = "Este metodo solo recibe numeros positivos";
    private static final String ERROR_MAX_SIZE = "Este metodo solo recibe 3 numeros positivos";

    public static void sortPositiveNumbers(Integer... numbers) throws Exception {
        validateSizeNumbers(numbers);
        validateNumbers(numbers);
        int max = Arrays.stream(numbers).mapToInt(v -> v).max().orElseThrow();
        int min = Arrays.stream(numbers).mapToInt(v -> v).min().orElseThrow();
        String message = "El numero mayor es " + max + " y el menor es " + min;
        System.out.println(message.trim());
    }

    private static void validateSizeNumbers(Integer[] numbers) throws SizeLimitExceededException {
        if (numbers.length > 3) {
            throw new SizeLimitExceededException(ERROR_MAX_SIZE);
        }
    }

    private static void validateNumbers(Integer[] numbers) {
        Arrays.stream(numbers).forEach(number -> {
            if (number < 0) {
                throw new IllegalArgumentException(ERROR_ONLY_POSITIVE_NUMBERS);
            }
        });
    }

    public static Integer[] generateArrayWithNumbersLessFive() {
        List<Integer> numbersGenerated = new ArrayList<>();
        Random randomClassNumber = new Random();
        for (int i = 0; i < 10; i++) {
            numbersGenerated.add(randomClassNumber.nextInt(10) + 1);
        }
        numbersGenerated = validateNumberGeneratedLessFive(numbersGenerated);
        Integer[] numbersResult = generatedArrayToList(numbersGenerated);
        return numbersResult;
    }

    private static List<Integer> validateNumberGeneratedLessFive(List<Integer> numbersGenerated) {
        return numbersGenerated
                .stream()
                .filter(number -> number < 5)
                .sorted()
                .collect(Collectors.toList());
    }

    private static Integer[] generatedArrayToList(List<Integer> numbersGenerated) {
        Integer[] numbersResult = new Integer[numbersGenerated.size()];
        numbersResult = numbersGenerated.toArray(numbersResult);
        return numbersResult;
    }

    public static int sum(int number_one, int number_two){
        return number_one + number_two;
    }

    public static int subtract(int number_one, int number_two){
        return number_one - number_two;
    }

    public static int multiplication(int number_one, int number_two){
        return number_one * number_two;
    }

    public static int divide(int number_one, int number_two){
        return number_one / number_two;
    }

}
